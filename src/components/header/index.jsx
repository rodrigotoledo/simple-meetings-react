import React, { Component } from 'react';

class Header extends Component {
  render(){
    return(
      <>
        <div className="row mt-4 justify-content-center align-items-center">
          <div className="col-12">
            <h2 id="application_title">Simple Meetings <small>Hello rodrigo@rtoledo.inf.br</small></h2>
            <div className="btn-group mt-2" role="group" aria-label="Application Actions">
              <a href="#" className="btn btn-primary">Dashboard/Create Meeting</a>
              <a href="#" className="btn btn-secondary">Create Room</a>
              <a href="#" className="btn btn-secondary">Logout</a>
            </div>
            <hr />
          </div>
        </div>
      </>
    )
  }
}

export default Header;