import React, { Component } from 'react'

export default class RoomItem extends Component {
  render() {
    return (
      <>
        <div className="card mb-2 bg-light" id="room-">
          <div className="card-body">
            <h4 className="card-title text-muted">
              Daily Meeting
              <small>-- 3 days ago: 10 meetings</small>
            </h4>
            <p className="card-text lead">
              <a href="#" className="btn btn-danger">Destroy</a>
            </p>
          </div>
        </div>
      </>
    )
  }
}
