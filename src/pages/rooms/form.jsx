import React from 'react';

export default function Form() {
  return (
    <>
      <form action="#" className="my-4">
        <div className="form-group">
          <input type="text" className="form-control" required="true" />
        </div>
        <div className="actions">
          <button type="submit" className="btn btn-primary">Submit</button>
        </div>
      </form>
    </>
  )
}
