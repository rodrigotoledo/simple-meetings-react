import React, { Component } from 'react'
import Header from '../../components/header';
import Form from './form';
import RoomList from '../../components/room_list';

export default class Rooms extends Component {
  render() {
    return (
      <>
        <Header />
        <h5>Dashboard for Rooms</h5>
        <Form />
        <div id="rooms">
          <RoomList />
        </div>
      </>
    )
  }
}
