import React from 'react';
import './styles.css';

export default function Form() {
  return (
    <>
      <form action="#">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="room_id" className="col-sm-4 col-form-label">Room</label>
            <div className="col-sm-8">
              <select name="room_id" id="room_id" prompt="Select a Room" style={{width: '100%'}} required="required" className="form-control"></select>
            </div>
          </div>
          <div className="col-md-6">
            <label htmlFor="title" className="col-sm-4 col-form-label">Title</label>
              <div className="col-sm-8">
                <input type="text" id="title" name="title" className="form-control" placeholder="Title..." required="required" />
              </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="date_start" className="col-sm-4 col-form-label">Date Start</label>
            <div className="col-sm-8">
              <select name="date_start" id="date_start" className="form-control" style={{width: 'auto', display: 'inline'}}>
                <option value="01">01</option>
              </select>
            </div>
          </div>
          <div className="col-md-6">
            <label htmlFor="date_end" className="col-sm-4 col-form-label">Date End</label>
            <div className="col-sm-8">
              <select name="date_end" id="date_end" className="form-control" style={{width: 'auto', display: 'inline'}}>
                <option value="01">01</option>
              </select>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="scheduled_at" className="col-sm-4 col-form-label">Scheduled at</label>
            <div className="col-sm-8">
              <select name="scheduled_at" id="scheduled_at" className="form-control" style={{width: 'auto', display: 'inline'}}>
                <option value="01">01</option>
              </select>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="actions">
              <button type="submit" className="btn btn-primary btn-save-meeting">Save</button>
            </div>
          </div>
        </div>
      </form>
    </>
  )
}
