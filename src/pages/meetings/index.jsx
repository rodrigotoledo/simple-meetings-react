import React, { Component } from 'react'
import Header from '../../components/header';
import Form from './form';
import Calendar from '../../components/calendar';

export default class Meetings extends Component {
  render() {
    return (
      <>
        <Header />
        <h5>Dashboard for Meetings</h5>
        <Form />
        <div id="calendar">
          <Calendar />
        </div>
      </>
    )
  }
}
