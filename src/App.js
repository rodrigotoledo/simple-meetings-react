import React from 'react';
import { Provider } from 'react-redux';
// import store from './store';

import Rooms from './pages/rooms';
import Meetings from './pages/meetings';

function App() {
  return (
    // <Provider store={store}>
      <div className="App">
        <div className="container h-100">
          <Meetings />
        </div>
      </div>
    // </Provider>
  );
}

export default App;
